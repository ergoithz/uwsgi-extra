
import sys
import os

def classmethod_application_proxy():
    importpath = os.getenv('CLASSMETHOD_APPLICATION_PROXY')
    module, prop = importpath.split(':')
    __import__(module)
    application = sys.modules[module]
    for part in prop.split('.'):
        application = getattr(application, part)
    return application

def instance_application_proxy():
    importpath = os.getenv('INSTANCE_APPLICATION_PROXY')
    module, prop = importpath.split(':')
    __import__(module)
    application_class = sys.modules[module]
    for part in prop.split('.'):
        application_class = getattr(application_class, part)
    return application_class()
