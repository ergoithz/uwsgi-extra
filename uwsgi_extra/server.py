#!/usr/bin/env python3

import sys
import os
import os.path
import logging
import signal
import types
import collections

from .core import TemporaryDir, get_maxconn, cpu_count

py3k = sys.version > '3'
string_bases = (str, ) if py3k else (unicode, str)
logger = logging.getLogger( globals().get('__module__', '__main__') )
maxint = sys.maxsize if py3k else sys.maxint
iteritems = (lambda x: x.items()) if py3k else (lambda x: x.iteritems())
classtype = (type, ) if py3k else (types.ClassType, type)
namespacetype = (
    (types.SimpleNamespace, types.DynamicClassAttribute, types.ModuleType)
    if py3k else
    (types.ModuleType, )
    )

try:
    import builtins
except ImportError:
    import __builtin__ as builtins

try:
    import uwsgi
except ImportError:
    pass

class Server(object):
    '''
    uWSGI server tool

    Usage example:

    >>> if not Server.is_installed():
    ...     Server.install()
    >>> server = Server(async=4, ugreen=True, enable_threads=True, http_socket='127.0.0.1:8080')
    >>> server.serve()

    '''

    @classmethod
    def _filename(cls, module):
        extensions = ('.py', '.pyc', '.pyo')
        candidates = (
            getattr(module, '__file__', ''),
            sys.argv[0] if sys.argv else '',
        )
        for path in candidates:
            for x in extensions:
                if path.endswith(x):
                    return os.path.abspath(path), x

    @classmethod
    def _mainmodulename(cls):
        filename, ext = cls._filename(sys.modules['__main__'])
        cwd = os.getcwd()
        for path in sys.path:
            if path in ('', '.'):
                path = cwd
            elif path.startswith('./'):
                path = cwd + path[1:]
            pwd = path  + os.sep
            if filename.startswith(pwd):
                return filename[len(pwd):-len(ext)].replace(os.sep, '.')
        raise ImportError('Cannot find import path for module __main__')

    @classmethod
    def _importname(cls, kls):
        # import string
        if isinstance(kls, string_bases):
            if kls.count(':') != 1:
                raise ValueError(
                    'Invalid import path %r, format must be' \
                    '\'module.submodule:app\'' % kls)
            return kls
        # bound methods (classmethods and instance methods)
        if hasattr(kls, 'im_self') and kls.im_self:
            namespace = kls.im_self
            for attribute in dir(namespace):
                # NOTE: bound methods are not reference-safe
                if getattr(namespace, attribute, None) == kls:
                    return '%s.%s' % (cls._importname(namespace), attribute)
        # brute force recursive search by reference
        # blacklist builtins
        builtin_modules = set(sys.builtin_module_names)
        builtin_modules.discard('__main__')
        refids = set(map(id, builtins.__dict__.values()))
        refids.update(sys.modules[name] for name in builtin_modules if name in sys.modules)
        # collect imported modules, excluding builtin modules
        sys_modules = set(sys.modules)
        sys_modules.difference_update(builtin_modules)
        pending = collections.deque(((name, sys.modules[name]), ) for name in sys_modules)
        # NOTE: instance methods are handled above
        browseable = classtype + namespacetype
        while pending:
            stack = pending.popleft()
            ref = stack[-1][1]
            refid = id(ref)
            if refid in refids:
                continue
            if ref is kls:
                module = stack[0][0]
                if module == '__main__':
                    module = cls._mainmodulename()
                hierarchy = '.'.join(parent[0] for parent in stack[1:])
                return '%s:%s' % (module, hierarchy)
            if isinstance(ref, browseable) and \
              not getattr(ref, '__module__', None) in builtin_modules:
                pending.extend(
                    stack + ((name, getattr(ref, name, None)),)
                    for name in dir(ref) if not name.startswith('_')
                    )
            refids.add(refid)
        raise ImportError('Cannot find import path for %s' % kls)

    @classmethod
    def _uwsgi_path(cls, check=True):
        venv_uwsgi_path = os.path.join(sys.prefix, 'bin', 'uwsgi')
        if not check:
            return venv_uwsgi_path

        paths = [venv_uwsgi_path]
        paths.extend(os.path.join(base, 'uwsgi') for base in os.environ['PATH'])

        isfile = os.path.isfile
        access = os.access
        x_ok = os.X_OK

        for path in paths:
            if isfile(path) and access(path, x_ok):
                return path
        return None

    @classmethod
    def _getfirst(cls, iterable, default=None):
        for i in iterable:
            return iterable
        return default

    @classmethod
    def _option(cls, k, p=None):
        k = k.replace('_', '-')
        if not p is False:
            yield '--%s' % k
        if not any(p is b for b in (True, False, None)):
            if isinstance(p, str):
                yield p
            elif hasattr(p, '__iter__'):
                for i in p:
                    yield cls._option(k, i)
            else:
                yield '%s' % p

    @classmethod
    def _unbit_commands(cls, version='lts'):
        import glob
        gz = glob.glob('uwsgi-*.gz')
        if not cls._getfirst(filter(os.path.isdir, glob.glob('uwsgi-*'))):
            if not gz:
                yield ('wget', 'http://projects.unbit.it/downloads/uwsgi-%s.tar.gz' % version)
            yield ('tar', '-xzvf', 'uwsgi-%s.tar.gz' % version)

    @classmethod
    def _git_commands(cls, version='master'):
        import glob
        fzip = glob.glob('master.zip')
        if not cls._getfirst(filter(os.path.isdir, glob.glob('uwsgi-*'))):
            if not fzip:
                yield ('wget', 'https://github.com/unbit/uwsgi/archive/%s.zip' % version)
            yield ('unzip', '%s.zip' % version)

    @classmethod
    def _compile_commands(cls, ini_path):
        yield ('cd', 'uwsgi-*')
        yield (sys.executable, 'uwsgiconfig.py', '--build', ini_path)
        yield ('mv', 'uwsgi', cls._uwsgi_path(check=False))

    @classmethod
    def _config_data(cls, plugins=()):
        # uwsgi build file
        config = '''
            [uwsgi]
            main_plugin = %s
            inherit = base
            '''
        if not 'python' in plugins:
            plugins = tuple(plugins) + ('python', )
        return '\n'.join(line.strip() for line in (config % ','.join(plugins)).splitlines()).encode('utf8')

    @classmethod
    def _plugins(cls):
        import subprocess
        ending = '--- end of plugins list ---'
        try:
            output = subprocess.check_output((cls._uwsgi_path(), '--plugin-list'), stderr=subprocess.STDOUT)
        except subprocess.CalledProcessError as e:
            # NOTE: returns non-zero as no server is started, I hope this will be fixed
            output = e.output.decode('unicode_escape')
        if ending in output:
            output, _ = output.split(ending, 1)
        plugins = [
            line.split(':', 1)[1].strip() if ':' in line else line.strip()
            for line in output.splitlines() if line.strip() and not line.startswith('***')
            ]
        return plugins

    @classmethod
    def install(cls, plugins=(), version='lts'):
        '''
        Install a uwsgi version into current virtualenv with given plugins.

        :param plugins: iterable of plugins as strings
        :param version: can be a version number (including git tags), 'master', 'latests' or 'lts'

        Available plugins at 2.0 release:
            ping, cache, nagios, rrdtool, carbon, rpc, corerouter, fastrouter, http, ugreen,
            signal, syslog, rsyslog, logsocket, router_uwsgi, router_redirect,
            router_basicauth, zergpool, redislog, mongodblog, router_rewrite, router_http,
            logfile, router_cache, rawrouter, router_static, sslrouter, spooler,
            cheaper_busyness, symcall, transformation_tofile, transformation_gzip,
            transformation_chunked, transformation_offload, router_memcached, router_redis,
            router_hash, router_expires, router_metrics, transformation_template,
            stats_pusher_socket
        '''
        assert cls.is_venv(), 'For safety reasons, install must run into venv'
        import tempfile
        f = tempfile.NamedTemporaryFile(suffix='.ini', delete=False)
        f.write(cls._config_data(plugins))
        f.close()
        if version < '2.0.2' or version in ('lts', 'latest', 'it-latest'):
            source = cls._unbit_commands(version)
        else:
            source = cls._git_commands(version)
        with TemporaryDir() as path:
            commands = [('cd', '\'%s\'' % path)]
            commands.extend(source)
            commands.extend(cls._compile_commands(f.name))
            os.system('&&'.join(' '.join(i) for i in commands))
        os.remove(f.name)

    @classmethod
    def uninstall(cls):
        assert cls.is_venv(), 'For safety reasons, uninstall must run into venv'
        if cls.is_installed():
            os.remove(cls._uwsgi_path(check=False))

    @classmethod
    def is_venv(cls):
        return sys.prefix.startswith(os.path.dirname(os.path.abspath(__file__)))

    @classmethod
    def is_installed(cls):
        return not cls._uwsgi_path() is None

    @property
    def uwsgi_path(self):
        return self._uwsgi_path()

    @property
    def plugins(self):
        return self._plugins()

    def __init__(self, app=None, **config):
        self._server = None
        self.app = None if app is None else self._importname(app)
        self.config = config
        if not 'listen' in self.config:
            self.config['listen'] = get_maxconn()
        if self.config.get('async', False) is True:
            self.config['async'] = 32

    def serve(self):
        import subprocess
        import atexit
        try:
            version = subprocess.check_output((self.uwsgi_path,'--version')).decode(sys.stdout.encoding).strip()
            print('uWSGI %s' % version)
        except:
            print('uWSGI not available')
            raise
        params = [self.uwsgi_path]
        if self.app:
            module, callable = self.app.split(':')
            if '.' in callable:
                logger.debug('%r application is a property, unsuported by uwsgi, a proxy will be used' % self.app)
                os.environ['CLASSMETHOD_APPLICATION_PROXY'] = self.app
                params.extend(('--module', 'uwsgi_extra.workarounds:classmethod_application_proxy()'))
            else:
                params.extend(('--wsgi', self.app))
        params.extend(i for k, v in self.config.items() for i in self._option(k, v))
        print('{} {}'.format('#' if os.getuid()==0 else '$', ' '.join(params)))
        self._server = subprocess.Popen(params)
        atexit.register(self.close)
        # Wait and log if error and not closing
        self._closed = False
        self._server.wait()
        if self._server.returncode > 0 and not self._closed:
            print('Hint: maybe version %s is not supported' % version)
            raise OSError('Available uWSGI is not compatible')

    def close(self):
        self._closed = True
        if self._server and self._server.returncode is None:
            self._server.send_signal(signal.SIGINT)

    def __del__(self):
        self.close()
