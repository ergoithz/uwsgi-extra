
import sys
import unittest
import threading
import contextlib
import time

try:
    import _thread as thread
except ImportError:
    import thread

try:
    import uwsgi
except ImportError:
    pass

from .async import request_id_middleware, sleep, wait_for_threads, request_id
from .server import Server

py3k = sys.version > '3'

class LiveServerTestCase(unittest.TestCase):
    def setUp(self):
        import time
        try:
            from urllib.request import urlopen
        except ImportError:
            from urllib import urlopen as old_urlopen
            urlopen = lambda url: contextlib.closing(old_urlopen(url))

        self.sleep = time.sleep
        self.urlopen = urlopen
        self.host = '127.0.0.1:5000'
        self.server = Server(self.application, async=True, ugreen=True, enable_threads=True, http_socket=self.host,
                             socket_timeout=10000)
        thread.start_new_thread(self.server.serve, ())
        self.sleep(2)

    def tearDown(self):
        self.server.close()

    def get(self, path=''):
        kwargs = {'timeout': 10000}  if py3k else {}
        with self.urlopen('http://%s%s' % (self.host, path), **kwargs) as o:
            return o.read().decode('utf-8')


class BrokenRequestIdTest(LiveServerTestCase):
    @staticmethod
    @request_id_middleware
    def application(environ, start_response):
        '''
        wsgi async application which shows our request_id and demonstrates uwsgi.request_id is broken
        '''
        start_response('200 OK', [('Content-Type', 'text/plain')])
        yield (' '*9048).encode('utf-8') # avoiding cache
        yield ('\n').encode('utf-8')
        yield (repr(uwsgi.__dict__)).encode('utf-8')
        for i in range(10):
            yield '\n{:<20} request_id:{} fd:{} id:{} wid:{}'.format(time.time(), request_id(), uwsgi.connection_fd(), uwsgi.request_id(), uwsgi.worker_id()).encode('utf8')
            sleep(1)

    def setUp(self):
        super(BrokenRequestIdTest, self).setUp()
        self.start_new_thread = thread.start_new_thread

    def get_rows(self, r, s=0):
        if s:
            self.sleep(s)
        r.append(i.strip().split() for i in self.get().splitlines() if i.strip())

    def get_parallel_rows(self, p):
        r = []
        pf = float(p)
        for i in range(p, 0, -1):
            self.start_new_thread(self.get_rows, (r, i/pf))
        while len(r) < p:
            self.sleep(0.1)
        return r

    def test_parallel(self):
        for proc in self.get_parallel_rows(1000):
            self.assertEqual(len({id for ts, reqid, fd, id, wid in proc}), 1)

    def test_bug(self):
        found = None
        for i in range(100):
            r = self.get_parallel_rows(10)
            for n, proc in enumerate(r):
                ids = {id for ts, reqid, fd, id, wid in proc}
                if len(ids) > 1:
                    found = proc
                    print('PROC %d returned mixed request_ids %r' % (n, ids))
                    break
            else:
                continue
            break
        self.assertNotEqual(found, None)


class FunctionTest(LiveServerTestCase):
    @staticmethod
    def application(environ, start_response):
        start_response('200 OK', [('Content-Type', 'text/plain')])
        output = ['failed']
        def target():
            time.sleep(1)
            output[0] = 'success'
            print('target finished')
        t = threading.Thread(target=target)
        t.start()
        wait_for_threads(t)
        yield '\n'.join(output).encode('utf-8')

    def test_wait_for_threads(self):
        self.assertEqual(self.get().strip(), 'success')


class ServerTest(unittest.TestCase):
    def test_importname(self):
        Server._importname(ImportableOldstyleClass)
        Server._importname(ImportableClass)
        Server._importname(ImportableClass.static_method)
        Server._importname(ImportableClass.class_method)

        with self.assertRaises(ImportError):
            Server._importname(ImportableClass.method)

        Server._importname(ImportableClass.NestedClass.class_method)
        Server._importname(ImportableClass.NestedClass.static_method)

        with self.assertRaises(ImportError):
            Server._importname(ImportableClass.NestedClass.method)

        instance = importable_class_instance
        Server._importname(instance.static_method)
        Server._importname(instance.class_method)
        Server._importname(instance.method)

        # NOTE: we cannot look into every instance in order to reach this cases
        # FIXME: needs further investigation
        # instance = importable_class_instance.nested_class_instance
        # Server._importname(instance.static_method)
        # Server._importname(instance.class_method)
        # Server._importname(instance.method)

        Server._importname(importable_callable_class_instance)

        Server._importname(importable_function)


class ImportableOldstyleClass:
    pass


class ImportableClass(object):
    def __init__(self):
        self.nested_class_instance = self.NestedClass()

    @staticmethod
    def static_method():
        pass

    @classmethod
    def class_method(cls):
        pass

    def method(self):
        pass

    class NestedClass(object):
        @staticmethod
        def static_method():
            pass

        @classmethod
        def class_method(cls):
            pass

        def method(self):
            pass


class ImportableCallableClass(object):
    def __call__(self):
        pass


def importable_function():
    pass


importable_class_instance = ImportableClass()
importable_callable_class_instance = ImportableCallableClass()


if __name__ == "__main__":
    if not Server.is_installed():
        Server.install()
    unittest.main()
