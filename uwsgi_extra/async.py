
import time
import functools
import threading

try:
    import _thread as thread
except ImportError:
    import thread

try:
    import uwsgi
    async_mode = 'async-cores' in uwsgi.opt
except ImportError:
    async_mode = False

if not async_mode:
    suspend = lambda: sleep(0.0001)
    sleep = time.sleep
    Lock = threading.Lock
else:
    suspend = uwsgi.suspend
    sleep = uwsgi.async_sleep

    class Lock(object):
        __slots__ = ('_locked',)
        def __init__(self):
            self._locked = False

        def acquire(self, blocking=True):
            if blocking:
                while self._locked:
                    suspend()
            elif self._locked:
                return False
            self._locked = True
            return True

        def release(self):
            if not self._locked:
                raise threading.ThreadError('release unlocked lock')
            self._locked = False

def wait_call(fnc, *args, **kwargs):
    output = [None, True]
    def inner():
        output[:] = fnc(*args, **kwargs), False
    thread.start_new_thread(inner, args=())
    while output[1]:
        suspend()
    return output[0]

def wait_for_value(value, *callables):
    while any(True for fnc in callables if fnc() != value):
        suspend()

def wait_for_threads(*threads):
    for thread in threads:
        wait_for_value(False, thread.is_alive)

def request_id_middleware(fnc):
    '''
    WSGI middleware which automatically calls `request_id_inc` on every request.

    Usage example:

    >>> def application(environ, start_response):
    ...     start_response('200 OK', [('Content-Type', 'text/html')])
    ...     return "request_id: {}".format(request_id()).encode('utf-8')
    >>> application = request_id_middleware(application)
    '''
    application = fnc
    @functools.wraps(fnc)
    def wrapped(environ, start_response):
        request_id_inc()
        return application(environ, start_response)
    return wrapped

def request_id():
    '''
    uwsgi.request_id is broken on async mode

    This function, used along with request_id_inc return a unique request id in current worker.
    '''
    fd = uwsgi.connection_fd()
    return fd * uwsgi.opt.get('async-cores', 64) + _fd_count[fd]

_fd_count = {}
def request_id_inc():
    '''
    uwsgi.request_id is broken on async mode

    This function, called on every request, ensures that ours request_id return value is unique for
    current worker.
    '''
    fd = uwsgi.connection_fd()
    count = _fd_count.get(fd, -1) + 1
    _fd_count[fd] = count % uwsgi.opt.get('async-cores', 64)
