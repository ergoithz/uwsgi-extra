import os
import sys
import tempfile
import shutil


class TemporaryDir(object):
    def __init__(self, suffix='', prefix='tmp', dir=None):
        self.path = tempfile.mkdtemp(suffix, prefix, dir)

    def __enter__(self):
        return self.path

    def __exit__(self, type, value, traceback):
        shutil.rmtree(self.path)

def cpu_count():
    '''
    Get number of CPUs

    :return: cpu count as integer
    '''
    try:
        import multiprocessing
        return multiprocessing.cpu_count()
    except (ImportError, NotImplementedError):
        pass
    return int(os.getenv("NUMBER_OF_PROCESSORS", 1))

def get_maxconn():
    '''
    Get maximum number of active connections

    :return: max active connections as integer
    '''
    import subprocess
    output = subprocess.check_output(('sysctl', 'net.core.somaxconn')).decode(sys.stdout.encoding)
    return int(output.rsplit('=', 1)[-1].strip())
